import React from 'react';
import styled from 'styled-components'

const SpanStyledNextComponent = (props) => <span {...props}>{props.children}</span>

export const SpanStyledNext = styled(SpanStyledNextComponent)`
  background: orange;
  color: white;
  font-size: 16px;
`

//

const CardComponent = (props) => (
  <div className={props.className}>
    <div/>
    <SpanStyledNext>Card Name</SpanStyledNext>
  </div>
)

export const CardStyledNext = styled(CardComponent)`
     background: white;
    width: 150px;
    height: 300px;
    display: flex;
    flex-direction: column;
    border: 1px solid black;
    margin: 10px;
    
    > div {
      background: grey;
      width: 100%;
      height: 100%;
    }
    
    ${SpanStyledNext} {
      color: red;
    }
`
