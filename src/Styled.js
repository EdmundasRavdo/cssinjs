import styled from 'styled-components'
import React from "react";

export const SpanStyled = styled.span`
  background: orange;
  color: white;
  font-size: 16px;
`


//

const StyledWrapper = styled.div`
    background: white;
    width: 150px;
    height: 300px;
    display: flex;
    flex-direction: column;
    border: 1px solid black;
    margin: 10px;
`

const StyledImage = styled.div`
    background: grey;
    width: 100%;
    height: 100%;
`

const RestyledSpan = styled(SpanStyled)`
  color: red;
`

export const CardStyled = (props) => (
  <StyledWrapper>
    <StyledImage/>
    <RestyledSpan>Card Name</RestyledSpan>
  </StyledWrapper>
)
