import React, {Component} from 'react';
import './App.css';
import {ThemeProvider} from 'react-jss'
import {Provider} from 'react-fela'
import {createRenderer} from 'fela'

import {SpanStyled, CardStyled} from './Styled'
import {SpanStyledNext, CardStyledNext} from './StyledNext'
import {SpanFela, CardFela} from './fela'
import {SpanJss, CardJss} from './Jss'
import styled from 'styled-components'

const Wrapper = styled.div`
  display: flex;
`

const Block = styled.div`
  display: flex;
  flex-direction: column;
  background: white;
  padding: 20px;
  margin: 20px;
  color: black;
  font-size: 14px;
`


const renderer = createRenderer()

class App extends Component {
  render() {
    return (
      <ThemeProvider theme={{}}>
        <Provider renderer={renderer}>
          <div className="App">
            <header className="App-header">
              <Wrapper>
                <Block>
                  Styled
                  <SpanStyled>Labas</SpanStyled>
                  <CardStyled/>
                </Block>
                <Block>
                  StyledNext
                  <SpanStyledNext>Labas</SpanStyledNext>
                  <CardStyledNext/>
                </Block>
                <Block>
                  Fela
                  <SpanFela>Labas</SpanFela>
                  <CardFela/>
                </Block>
                <Block>
                  Jss
                  <SpanJss>Labas</SpanJss>
                  <CardJss/>
                </Block>
              </Wrapper>
            </header>
          </div>
        </Provider>
      </ThemeProvider>
    );
  }
}

export default App;
