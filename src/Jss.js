import React from 'react'
import injectSheet from 'react-jss'

const SpanComponent = ({className, ...props}) => (
  <span {...props}
        className={className ? `${props.classes.span} ${className}` : props.classes.span}
  >
    {props.children}
  </span>
)

const styles = () => ({
  span: {
    background: 'orange',
    color: 'white',
    fontSize: 16,
  },
})

export const SpanJss = injectSheet(styles)(SpanComponent)

//

const CardComponent = (props) => (
  <div className={props.classes.card}>
    <div className={props.classes.image}></div>
    <SpanJss className={props.classes.title}>Card Name</SpanJss>
  </div>
)

const cardStyles = () => ({
  card: {
    background: 'white',
    width: 150,
    height: 300,
    display: 'flex',
    flexDirection: 'column',
    border: '1px solid black',
    margin: 10
  },
  image: {
    background: 'grey',
    width: '100%',
    height: '100%',
  },
  title: {
    color: 'red',
  }
})

export const CardJss = injectSheet(cardStyles)(CardComponent)
