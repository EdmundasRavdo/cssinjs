import React from 'react'
import {connect} from 'react-fela'

const SpanComponent = ({className, ...props}) => (
  <span {...props}
        className={className ? `${props.styles.span} ${className}` : props.styles.span}
  >
    {props.children}
  </span>
)

const rules = () => ({
  span: {
    background: 'orange',
    color: 'white',
    fontSize: '16px',
  }
})

export const SpanFela = connect(rules)(SpanComponent)

//

const CardComponent = (props) => (
  <div className={props.styles.card}>
    <div className={props.styles.image}></div>
    <SpanFela className={props.styles.title}>Card Name</SpanFela>
  </div>
)

const cardStyles = () => ({
  card: {
    background: 'white',
    width: '150px',
    height: '300px',
    display: 'flex',
    flexDirection: 'column',
    border: '1px solid black',
    margin: '10px'
  },
  image: {
    background: 'grey',
    width: '100%',
    height: '100%',
  },
  title: {
    color: 'red',
  }
})

export const CardFela = connect(cardStyles)(CardComponent)
